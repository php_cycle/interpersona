<?php

class apiController
{
    public $adminModel;
    public $pagesModel;
    public $mediaModel;
    public $usersModel;

    public function __construct($para)
    {
        header('Content-Type: application/json');

        if ( ! method_exists($this, $para[0]) )
        {
            header('HTTP/1.0 404 Not Found');
            exit ( json_encode(['result' => 'Method does not exist']) );
        }

        else
        {
            /**
             * Escape SQL inj
             */
            $this->adminModel = new adminModel();

            $this->{$para[0]}();

            /**
             * After every update, recache pages. Could be deprecated someday
             */
            $this->pagesModel = new pagesModel();
            $this->pagesModel->recache_pages(false);

        }
    }

    public function login()
    {
        $_SESSION['logon'] = $this->adminModel->login( @ $_POST['email'], @ $_POST['password'] );

        echo $_SESSION['logon'] !== false
            ? json_encode(['result' => true])
            : json_encode(['result' => false]);
    }

    public function logout()
    {
        unset($_SESSION['logon']);
        echo json_encode(['result' => true]);
    }

    public function remove_page()
    {
        if ( empty(@ $_POST['id']) ) return;

        $this->pagesModel = new pagesModel();
        $this->pagesModel->remove_page($_POST['id']);

        echo json_encode(['result' => true]);
    }

    public function edit_page()
    {
        if ( empty(@ $_POST['id']) ) return;

        $this->pagesModel = new pagesModel();
        $this->pagesModel->edit_page( @ $_POST['id'], @ $_POST['column'], @ $_POST['value']  );

        echo json_encode(['result' => true]);
    }

    public function add_page()
    {
        $this->pagesModel = new pagesModel();
        $this->pagesModel->add_page($_POST);

        echo json_encode(['result' => true]);
    }

    public function upload_image()
    {
        if ( $_POST )
        {
            $image_size = getimagesize("../src/admin/view/assets/img/upload/" . basename($_POST['url']));

            $_POST['size'] = $image_size;

            echo json_encode($_POST);

            return;
        }

        $file_name = md5(time() . $_FILES['image']['tmp_name']);

        move_uploaded_file($_FILES['image']['tmp_name'], "../src/admin/view/assets/img/upload/{$file_name}.png");

        $image_size = getimagesize("../src/admin/view/assets/img/upload/{$file_name}.png");

        echo json_encode([
            'url' => "{$_SERVER['HTTP_ORIGIN']}/img/upload/{$file_name}.png",
            'size' => $image_size
        ]);
    }

    public function get_media()
    {
        $this->mediaModel = new mediaModel();

        echo json_encode([
            'head' => $this->mediaModel->head,
            'data' => $this->mediaModel->get_files(@$_POST['filter'], @$_POST['order'])
        ]);
    }
    
    public function update_media()
    {
        $this->mediaModel = new mediaModel();
        
        $this->mediaModel->update_media($_POST['id'], $_POST['column'], $_POST['value']);
        
        echo json_encode([
            'result' => true
        ]);
    }

    public function upload_media()
    {
        $this->mediaModel = new mediaModel();

        $filenames = [];

        print_r($_FILES);

        foreach ( (array) $_FILES as $file )
            $filenames[] = $this->mediaModel->add_file($file);

        echo json_encode([
            'data' => $filenames
        ]);
    }

    public function get_settings()
    {
        $this->settingsModel = new settingsModel();

        echo json_encode([
            'head' => $this->settingsModel->head,
            'data' => $this->settingsModel->get_settings(@$_POST['filter'], @$_POST['order'])
        ]);
    }

    public function update_settings()
    {
        $this->settingsModel = new settingsModel();

        $this->settingsModel->update_settings($_POST['id'], $_POST['column'], $_POST['value']);

        echo json_encode([
            'result' => true
        ]);
    }

    public function get_users()
    {
        $this->usersModel = new usersModel();

        echo json_encode([
            'head' => $this->usersModel->head,
            'data' => $this->usersModel->get_users(@$_POST['filter'], @$_POST['order'])
        ]);
    }

    public function update_users()
    {
        $this->usersModel = new usersModel();

        $this->usersModel->update_users($_POST['id'], $_POST['column'], $_POST['value']);

        echo json_encode([
            'result' => true
        ]);
    }
    
    public function insert_users()
    {
        $this->usersModel = new usersModel();

        $this->usersModel->insert_user();

        echo json_encode([
            'result' => true
        ]);
    }
}
