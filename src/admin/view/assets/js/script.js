$(document).ready(function()
{
    $('.logout').click(function() {
        $.ajax({
            'url' : window.location.protocol + '//' + window.location.host + '/admin/api/logout',
            method : 'POST'
        }).done(function() { location.reload() })
    });

    /**
     * Edit-page
     */
    {
        $('.page-header .btn').click(function()
        {
            $('.add_page.modal').modal();
        })

        $('.add_page.modal .btn.submit').click(function(e)
        {
            var data = {};

            $('.add_page.modal input[name]').each(function(e)
            {
                data[$(this).attr('name')] = $(this).val()
            })

            $.ajax({
                'url' : window.location.protocol + '//' + window.location.host + '/admin/api/add_page',
                method : 'POST',
                data : data
            }).done(function(data)
            {
                if ( data.result === true )
                {
                    success('Page created succesfully');
                    $('.add_page.modal [data-dismiss=modal]').click();
                }
            })
        });

        $('.pages-list .remove-page').click(function(e)
        {
            $.ajax({
                'url' : window.location.protocol + '//' + window.location.host + '/admin/api/remove_page',
                method : 'POST',
                data : { id: $($(e.target).parent().parent().parent()).attr('page-id') }
            }).done(function(data)
            {
                if ( data.result === true )
                    $($(e.target).parent().parent().parent()).fadeOut();
            })
        })

        var pageEditAction = function(e)
        {
            $('.pages-list *').removeClass('success');
            $('.pages-list *').removeClass('fail');

            var send_timer = 'edit_page-'
                + $($(e.target).parent().parent()).attr('page-id')
                + '-' + $(e.target).attr('column');

            clearTimeout(window[send_timer]) || void(0);

            window[send_timer] = setTimeout(function()
            {
                $.ajax({
                    'url' : window.location.protocol + '//' + window.location.host + '/admin/api/edit_page',
                    method : 'POST',
                    data :
                    {
                        id: $($(e.target).parent().parent()).attr('page-id'),
                        column: $(e.target).attr('column'),
                        value: ( ($(e.target).attr('type') == 'checkbox' && $(e.target).is(':checked') && 'Y' || $(e.target).attr('type') == 'checkbox' && 'N') || $(e.target).val() )
                    }
                }).done(function(data)
                {
                    if ( data.result === true )
                        $(e.target).parent().addClass('success');
                    else
                        $(e.target).parent().addClass('fail');
                })
            }, 500);
        };

        $('.pages-list input[type=text].form-control').keydown(pageEditAction);
        $('.pages-list input[type=checkbox].form-control').change(pageEditAction);
    }

    /**
     * Add files
     * **/
    {
        $('.page-header .btn').click(function()
        {
            $('.add_file.modal').modal();
        })

        $('.modal.add_file *').change(function()
        {
            $('.modal.add_file *').unbind();

            $('.modal.add_file input[readonly]').click(function () { $(this).select() })
        })

        // Upload
        $('.modal.add_file input[multiple]').change(function()
        {
            var data = new FormData();
            jQuery.each($(this)[0].files, function(i, file) {
                data.append('file-'+i, file);

                console.log('FILEF OUT');
            });

            $('.modal.add_file form').ajaxSubmit({
                target : '.modal.add_file form',
                type : 'POST',

                uploadProgress : function(event, position, total, percentComplete)
                {
                    console.log(percentComplete)
                },

                success : function()
                {
                    $('.modal.add_file form')[0].reset()
                },

                resetForm : true
            });

            return false;
        })
    }

    /**
     * Table editing system
     */

    window.table_edit = function(table, filter, order)
    {
        $(table).find('*').unbind();
        $(table).find('thead, tbody').html('');

        $.ajax({
            method : 'POST',
            url : window.location.protocol + '//' + window.location.host + '/admin/api/get_' + window['table'],

            data :
            {
                'filter' : filter,
                'order' : order,
            },

            success : function(data)
            {
                var head = [];

                for ( var i in data.head )
                {
                    data.head[i] != null && head.push(data.head[i]);

                    if ( data.head[i]['visible'] === false ) continue;

                    $(table).find('thead').append('<th>' + i + '</th>');
                }

                var tbody = '';

                var groups = undefined;

                for ( var i in data.data )
                {
                    if ( data.data[i]['group'] != groups )
                    {
                        groups = data.data[i]['group'];
                        tbody += '<h3>' + groups + '</h3>';
                    }

                    tbody += '<tr ' + window['table'] + '_id="'+ data.data[i][window['table'] + '_id'] +'">';

                    // Render element
                    var c = 0;

                    for ( var j in data.data[i] )
                    {
                        if ( head[c] && head[c]['visible'] === false )
                        {
                            c++;
                            continue
                        };

                        tbody += '<td unique="' + Math.random() + '">';

                        var record = data.data[i][j],
                            content = record,
                            el = head[c];

                        /**
                         * Merge with record's "head" JSON column if it's set
                         */
                        var elMerge = {};

                        for ( var k in el ) elMerge[k] = el[k];

                        if ( data.data[i]['head'] && data.data[i]['head'] != '{}' && (headJSON = JSON.parse(data.data[i]['head'])) )
                            if ( typeof el['raw'] == 'undefined' )
                                for ( var k in headJSON ) elMerge[k] = headJSON[k];

                        /**
                         * Check element types
                         */
                        if ( typeof elMerge['raw'] != 'undefined' )
                            tbody += elMerge['raw'].replace('{content}', content);

                        else if ( typeof elMerge['element'] != 'undefined' )
                        {
                            var element = document.createElement(el.element);

                            for ( var x in el )
                                if ( x != 'element' )
                                    $(element).attr(x, elMerge[x]);

                            $(element).attr('value', content);

                            if ( elMerge['type'] == 'checkbox' )
                                if ( record == 'Y' )
                                    $(element).attr('checked', 'checked');

                            tbody += element.outerHTML;
                        }

                        else tbody += content;

                        tbody += '</td>';

                        c++;
                    }

                    tbody += '</tr>';
                }

                $(table).find('tbody').html(tbody);

                /**
                 * Bind change events to elements
                 */
                var change = function(id, column, value)
                {
                    $.ajax({
                        method : 'POST',
                        url : window.location.protocol + '//' + window.location.host + '/admin/api/update_' + window['table'],

                        data :
                        {
                            'id' : id,
                            'column' : column,
                            'value' : value
                        },

                        success : function()
                        {
                            return true
                        }
                    });

                    return true;
                };

                $(table).find('input[type=text], input[type=password], input[type=email], textarea').keydown(function()
                {
                    var el = $(this);

                    $(el).parent().removeClass('success fail');

                    clearTimeout( window[$(el).parent().attr('unique')] );

                    window[$(el).parent().attr('unique')] = setTimeout(function()
                    {
                        var response = change(
                            $(el).parent().parent().attr(window['table'] + '_id'),
                            $(el).attr('column'),
                            $(el).val()
                        );

                        if ( response )
                            $(el).parent().addClass('success');
                    }, 300);
                });

                $(table).find('button.btn-danger').click(function()
                {
                    var el = $(this);

                    $(el).parent().removeClass('success fail');

                    clearTimeout( window[$(el).parent().attr('unique')] );

                    window[$(el).parent().attr('unique')] = setTimeout(function()
                    {
                        var response = change(
                            $(el).parent().parent().attr(window['table'] + '_id'),
                            $(el).attr('column'),
                            'Y'
                        );

                        if ( response )
                            table_edit($('table.table'));
                    });
                });

                $(table).find('input[type=checkbox]').change(function()
                {
                    var el = $(this);

                    $(el).parent().removeClass('success fail');

                    clearTimeout( window[$(el).parent().attr('unique')] );

                    window[$(el).parent().attr('unique')] = setTimeout(function()
                    {
                        var response = change(
                            $(el).parent().parent().attr(window['table'] + '_id'),
                            $(el).attr('column'),
                            ( ($(el).attr('type') == 'checkbox' && $(el).is(':checked') && 'Y' || $(el).attr('type') == 'checkbox' && 'N') || $(el).val() )
                        );

                        if ( response )
                            $(el).parent().addClass('success');
                    }, 300);
                })
            }
        })
    };

    if ( $('table.dynamic-table.media').length > 0 )
        window.table = 'media',
            table_edit($('table.table'));

    if ( $('table.dynamic-table.settings').length > 0 )
        window.table = 'settings',
            table_edit($('table.table'));

    if ( $('table.dynamic-table.users').length > 0 )
        window.table = 'users',
            table_edit($('table.table'));

    $('button.insert-record').click(function()
    {
        $.ajax({
            'url' : window.location.protocol + '//' + window.location.host + '/admin/api/insert_' + window.table,
            method : 'POST',

            success : function(data)
            {
                if ( data.result === true )
                    table_edit($('table.table'));
            }
        })
    });
});