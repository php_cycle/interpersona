<?php

class pagesModel
{
    public $MyCRUD;

    public $data;
    public $list;

    public function __construct($slug = null)
    {
        $pages_columns = [
            'pages.title.VARCHAR(1000)',
            'pages.slug.VARCHAR(1000)',
            'pages.position.INT(11)',
            'pages.visible.ENUM(\'Y\', \'N\') DEFAULT \'Y\'',
            'pages.protected.ENUM(\'Y\', \'N\') DEFAULT \'Y\'',
            'pages.content.LONGTEXT',
            'pages.created.DATETIME DEFAULT CURRENT_TIMESTAMP',
            'pages.status.ENUM(\'Y\', \'N\') DEFAULT \'N\'',
        ];

        $this->MyCRUD = new \app\MyCRUD($GLOBALS['mysqli']['interpersona'], [
            'tables' => 'pages',
            'columns' => $pages_columns
        ]);

        if ( ! is_null($slug) )
            $this->data = @ $this->MyCRUD->pull([
                'filter' => "UPPER(pages.slug) = UPPER('{$slug}') AND pages.status = 'Y'"
            ], true)[0];
    }

    public function pageList()
    {
        $this->list = $this->MyCRUD->pull([
            'filter' => "pages.status = 'Y'",
            'order' => 'pages.position ASC'
        ], true);

        return $this->list;
    }

    public function remove_page($id)
    {
        $this->MyCRUD->update('status', "pages.pages_id = '{$id}'", 'N');

        $this->recache_pages(false);
    }

    public function edit_page($id, $column, $value)
    {
        $this->MyCRUD->update($column, "pages.pages_id = '{$id}'", $value);

        if ( $column == 'content' )
        {
            $backup = new \app\MyCRUD($GLOBALS['mysqli']['interpersona'], [
                'tables' => 'pages_updates',
                'columns' => [
                    'pages_updates.parent.INT(11)',
                    'pages_updates.content.LONGTEXT',
                    'pages_updates.created.DATETIME DEFAULT CURRENT_TIMESTAMP',
                ]
            ]);

            $backup->insert([
                'pages_updates.parent' => $id,
                'pages_updates.content' => $value
            ], true);
        }

        self::recache_pages(false);
    }

    public function add_page($data = array())
    {
        foreach ( $data as $name => $value )
        {
            $data["pages.{$name}"] = $value;
            unset($data[$name]);
        }

        $this->MyCRUD->insert($data, true);

        $this->recache_pages(false);
    }

    /**
     * Clear out all page keys
     * Re-render and recache the pages
     */
    public function recache_pages($insert = true)
    {
        if ( empty($this->list) ) $this->pageList();

        PHPCycle::load_project('main_site', 'controller');

        $indexController = new indexController();

        $GLOBALS['phpcycle']['project'] = 'main_site';

        foreach ( $this->list as $i )
            foreach ( ['page', 'pageList', 'pageHTML'] as $j )
            {
                @ Memc::delete("{$j}-admin-{$i['slug']}");
                @ Memc::delete("{$j}-guest-{$i['slug']}");
            }

        /**
         * Decide whether to renew the cache
         */
        if ( $insert === true )
        {
            sleep(1);

            foreach( $this->list as $i )
            {
                @ $indexController->slug([$i['slug']], [], true, false);
                @ $indexController->slug([$i['slug']], [], true, true);
            }
        }

        /**
         * Manually delete admin pages
         */
        Memc::delete('adminPage-pages');

        $GLOBALS['phpcycle']['project'] = 'admin';
    }
}