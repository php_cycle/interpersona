<?php

class adminModel
{
    private $MyCRUD;

    public $data;
    public $list;

    public function __construct($schema = true)
    {
        if ( $schema === true )
            $this->MyCRUD = new \app\MyCRUD($GLOBALS['mysqli']['interpersona'], [
                'tables' => 'admin',
                'columns' => [
                    'admin.email.VARCHAR(500)',
                    'admin.password.VARCHAR(500)',
                    'admin.name.VARCHAR(500)',
                    'admin.status.ENUM(\'Y\', \'N\') DEFAULT \'Y\'',
                ]
            ]);
    }

    public function getAllUsers()
    {
        return $this->list = @ $this->MyCRUD->pull([
            'filter' => "admin.status = 'Y'",
        ], true);
    }

    public function login($email, $password)
    {
        $this->list = Memc::get("login-{$email}");

        if ( ! ( $this->list && $this->list['password'] == $password ) )
        {
            if ( ! $this->MyCRUD )
                $this->__construct(true);

            $this->list = @ $this->MyCRUD->pull([
                'filter' => "admin.status = 'Y' AND UPPER(admin.email) = UPPER('{$email}') AND admin.password = '{$password}'",
            ], true)[0];

            if ( $this->list )
                Memc::set([
                    [ "login-{$email}", $this->list ]
                ]);
        }

        return isset($this->list['admin_id'])
            ? $this->list : false;
    }
}