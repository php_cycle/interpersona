<?php

class mediaModel
{
    private $MyCRUD;

    public $files;

    public $head =
        [
            '#' => [],
            'Filename' => [
                'element' => 'input',
                'type' => 'text',
                'placeholder' => 'Filename',
                'class' => 'form-control',

                'column' => 'filename'
            ],
            'Slug' => [
                'element' => 'input',
                'type' => 'text',
                'placeholder' => 'Slug',
                'class' => 'form-control',

                'column' => 'slug'
            ],
            'Password' => [
                'element' => 'input',
                'type' => 'text',
                'placeholder' => 'Password (leave empty otherwise)',
                'class' => 'form-control',

                'column' => 'password'
            ],
            'Accessible' => [
                'element' => 'input',
                'type' => 'checkbox',
                'class' => 'form-control',

                'column' => 'status'
            ],
            'Delete' => [
                'element' => 'button',
                'class' => 'btn btn-danger glyphicon glyphicon-remove',

                'column' => 'delete'
            ],
        ];

    public function __construct($schema = true)
    {
        $this->MyCRUD = new \app\MyCRUD($GLOBALS['mysqli']['interpersona'], [
            'tables' => 'media',
            'columns' => [
                'media.filename.VARCHAR(500)',
                'media.slug.VARCHAR(500)',
                'media.password.VARCHAR(500)',
                'media.status.ENUM(\'Y\', \'N\') DEFAULT \'Y\'',
                'media.delete.ENUM(\'Y\', \'N\') DEFAULT \'N\'',
            ]
        ]);
    }

    public function add_file($file, $slug = '', $password = '')
    {
        $enum = $filename = null;

        while ( true )
        {
            $enumAdd = ($enum > 0 ? "-{$enum}" : $enum);
            
            $filename = pathinfo($file['name'], PATHINFO_FILENAME) . $enumAdd . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
            
            if ( ! file_exists("../src/admin/view/assets/media/{$filename}") ) break;

            (int) $enum++;
        }

        $this->MyCRUD->insert([
            'media.filename' => $filename,
            'media.slug' => @ $slug,
            'media.password' => $password
        ]);
        
        move_uploaded_file($file['tmp_name'], "../src/admin/view/assets/media/{$filename}");

        return $filename;
    }

    public function get_files($filter, $order)
    {
        if ( !empty($filter) ) $filter = "AND {$filter}";
        if ( !empty($order) ) $filter = ", {$order}";

        return $this->files = $this->MyCRUD->pull([
            'filter' => "media.delete = 'N' {$filter}",
            'order' => "media.media_id DESC {$order}"
        ], true);
    }

    public function update_media($id, $column, $value)
    {
        $this->MyCRUD->update($column, "media.media_id = '{$id}'", $value);
    }
}