<?php

class usersModel
{
    private $MyCRUD;

    public $data;
    public $data_clean = [];

    public $head =
        [
            '#' => [],
            'Email' => [
                'element' => 'input',
                'type' => 'email',
                'placeholder' => 'Email address',
                'class' => 'form-control',

                'column' => 'email'
            ],
            'Password' => [
                'element' => 'input',
                'type' => 'text',
                'placeholder' => 'Password',
                'class' => 'form-control',

                'column' => 'password'
            ],
            'Password' => [
                'element' => 'input',
                'type' => 'text',
                'placeholder' => 'Password',
                'class' => 'form-control',

                'column' => 'password'
            ],
            'Delete' => [
                'element' => 'button',
                'class' => 'btn btn-danger glyphicon glyphicon-remove',

                'column' => 'delete'
            ],
        ];

    public function __construct($schema = true)
    {
        $this->MyCRUD = new \app\MyCRUD($GLOBALS['mysqli']['interpersona'], [
            'tables' => 'users',
            'columns' => [
                'users.email.VARCHAR(500)',
                'users.password.VARCHAR(500)',
                'users.delete.ENUM(\'Y\', \'N\') DEFAULT \'N\''
            ]
        ]);
    }

    public function get_users($filter = '', $order = '')
    {
        if ( !empty($filter) ) $filter = "AND {$filter}";
        if ( !empty($order) ) $filter = ", {$order}";

        return $this->data = $this->MyCRUD->pull([
            'filter' => "users.delete = 'N' {$filter}",
            'order' => "users.users_id DESC {$order}"
        ], true);
    }

    public function update_users($id, $column, $value)
    {
        $this->MyCRUD->update($column, "users.users_id = '{$id}'", $value);
    }

    public function insert_user($email = '', $password = '')
    {
        $this->MyCRUD->insert([
            'users.email' => $email,
            'users.password' => $password
        ], true);
    }
}