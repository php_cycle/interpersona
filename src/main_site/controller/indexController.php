<?php

class indexController
{
    public function __construct()
    {
        PHPCycle::load_project('admin');

        $GLOBALS['twig_filters']['md5'] = function($str) { return md5($str); };
    }

    public function phpinfo()
    {
        phpinfo();
    }

    public function slug($para, $attr, $return = false, $admin = null)
    {
        $page_type = @ is_array($_SESSION['logon']) ? 'admin' : 'guest';

        // Reconsider page type in case it has been specified manually
        if ( $admin === true )
            $page_type = 'admin';
        else if ( $admin === false )
            $page_type = 'guest';

        // Close with Memcache in case it's cached
        if ( $pageHTML = Memc::get("pageHTML-{$page_type}-{$para[0]}") )
            if ( $return === false )
                exit ( $pageHTML );
            else
                return $pageHTML;

        $page = Memc::get("page-{$para[0]}");
        $pageList = Memc::get("pageList-{$para[0]}");

        if ( ! $page ) $page = new pagesModel($para[0]);
        if ( ! $pageList ) $page->pageList();

        $template_name = empty($para[0]) ? 'index' : $para[0];

        if ( ! ($page->data && is_file("../src/main_site/view/templates/pages/{$template_name}.html.twig") && $template = "pages/{$template_name}.html.twig") )
            $template = $page->data ? 'pages/page.html.twig' : 'pages/404.html.twig';

        $settingsModel = new settingsModel();

        $pageHTML = @ Twig::render($template, [
            'theme' => '1.0',
            'slug' => $para[0],
            'page' => $page->data,
            'pageList' => $page->list,
            'settings' => $settingsModel->get_settings_clean(),
            'logon' => ($admin === false ? false : $_SESSION['logon']),
            'pdf_download' => Memc::get('pdf_download')
        ]);
        

        // Respond 404 if page wasn't found
        if ( ! $page->data )
        {
            header('HTTP/1.0 404 Not Found');
            exit ($pageHTML);
        }

        Memc::set([
            [ "page-{$page_type}-{$para[0]}", $page, 60*60*24 ],
            [ "pageList-{$page_type}-{$para[0]}", $pageList, 60*60*24 ],
            [ "pageHTML-{$page_type}-{$para[0]}", $pageHTML, 60*60*24 ]
        ]);

        // Decide whether to return the content instead or not
        if ( $return === false )
            exit ( $pageHTML );
        else
            return $pageHTML;
    }
}