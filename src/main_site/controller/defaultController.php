<?php

class defaultController
{
    public static function asset_not_found($para)
    {
        header('HTTP/1.0 404 Not Found');

        echo Twig::render('pages/404.html.twig', [
            'theme' => '1.0',
            'slug' => $para[0]
        ], 60*60*24);

        exit;
    }
}