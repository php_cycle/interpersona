<?php

class apiIndexController
{
    public function __construct($para)
    {
        header('Content-Type: application/json');

        if ( ! method_exists($this, $para[0]) )
        {
            header('HTTP/1.0 404 Not Found');
            exit ( json_encode(['result' => 'Method does not exist']) );
        }

        else
        {
            PHPCycle::load_project('admin');
            
            $this->{$para[0]}();
        }
    }

    public function sendmail()
    {
        $settingsModel = new settingsModel();
        
        $PHPMailer = new PHPMailer;

        $PHPMailer->isSMTP();
        $PHPMailer->Host = 'smtp.gmail.com';
        $PHPMailer->SMTPAuth = true;
        $PHPMailer->Username = 'phpcycle@gmail.com';
        $PHPMailer->Password = 'x7zTAsHt';
        $PHPMailer->SMTPSecure = 'ssl';
        $PHPMailer->Port = 465;
        $PHPMailer->isHTML(false);

        $PHPMailer->addAddress($settingsModel->get_setting('Website Email'), 'Interpersona');

        $PHPMailer->setFrom($_POST['email'], $_POST['name']);
        $PHPMailer->addReplyTo($_POST['email'], 'Information');

        $PHPMailer->Subject = "Interpersona contact form: {$_POST['subject']}";
        $PHPMailer->Body    = 'Name: ' . $_POST['name'] . "\n\nMessage:\n{$_POST['message']}";

        if ( ! $PHPMailer->send() ) exit;

        echo json_encode(['result' => true]);
    }
}
