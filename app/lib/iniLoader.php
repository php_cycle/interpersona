<?php

namespace PHPCycle;


class iniLoader
{
    /**
     * Recursively load ini files from a given path
     * @param $path
     */
    public function load_files($path)
    {
        foreach ( \PHPCycle::glob("{$path}/*.json") as $subject )
            if ( is_file($subject) )
                $GLOBALS['phpcycle']['iniContents'][$subject] = json_decode ( \PHPCycle::read_file($subject), true );
            else
                $this->load_files($subject);
    }

    /**
     * Merge iniContents array recursively with $GLOBALS root
     * -Then unset iniContents array
     */
    public static function merge_ini()
    {
        foreach ( @ (array) $GLOBALS['phpcycle']['iniContents'] as $contents )
            foreach ( $contents as $key => $value )
                if ( array_key_exists($key, $GLOBALS) )
                    $GLOBALS[$key] = array_merge_recursive((array) $value, (array) $GLOBALS[$key]);
                else
                    $GLOBALS[$key] = $value;

        unset($GLOBALS['phpcycle']['iniContents']);
    }
}