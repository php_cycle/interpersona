<?php

/**
 * Instantiate object and connect with Memcache server(s)
 */
$GLOBALS['memcache'] = new Memcache;

foreach ( $GLOBALS['memcache_servers'] as $i )
    $GLOBALS['memcache']->addserver( $i[0], $i[1], true, ! isset($i[2]) ? 100 : $i[2] );

class Memc
{
    private static function checksum($key)
    {
        return md5( "PHPCYCLE" . $_SERVER['DOCUMENT_ROOT'] . $key );
    }

    /**
     * Sets values
     * @param array $data [ [key, value, expire||null, flag_compressed||null] ]
     */
    public static function set($data = array())
    {
        foreach ( $data as $i )
        {
            $i[0] = self::checksum($i[0]);

            $j = (array) $GLOBALS['memcache']->get($i[0]);

            unset($j[0]);

            $j['created'] = @ $j['created'] ? @ $j['created'] : time();
            $j['modified'] = time();
            $j['value'] = $i[1];

            @ $GLOBALS['memcache']->set( $i[0], $j, $i[3], ($i[2]?$i[2]:0) );
        }
    }

    /**
     * Get entire objects
     * @param array $key
     * @return array
     */
    public static function get($key, $is_array = false)
    {
        $data = $GLOBALS['memcache']->get( self::checksum($key) );

        if ( $data !== false )
            return $is_array === false
                ? $data['value']
                : $data;
        else
            return false;
    }

    /**
     * Delete list of keys
     * @param array $keys
     */
    public static function delete($keys = array())
    {
        foreach ( (array) $keys as $i )
            $GLOBALS['memcache']->replace( self::checksum($i), false, 0, 1 );
    }
}