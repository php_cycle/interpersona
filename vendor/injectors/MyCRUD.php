<?php

/**
 * MyCRUD version 0.01
 * CRUD system & schema adjuster
 * By Chester Abrahams
 */

namespace app;


class MyCRUD
{
    private $db_con;
    private $db_name;

    protected $data;
    protected $schema;
    protected $tables;

    protected $debug = false;
    protected $pullRequest;

    private $store_tables;
    private $store_columns;

    // A consistent variable containing the data produced by get()
    public $get;

    // A string which contains a collection of SQL queries. This will be executed when you push
    public $queryCollection;

    public function __construct($resource, $pull = null)
    {
        // Connect to database resource
        if ( isset($resource['db_con'])  ) null;

        else if ( is_array($resource) )
        {
            $con = $resource;

            $GLOBALS['mysqli'][@$con['db_alias'] ? $con['db_alias'] : $con['db_name']] = array(
                'db_name' => $con['db_name'],
                'db_con' => @ new \mysqli(
                    $con['db_host'],
                    $con['db_user'],
                    $con['db_pass'],
                    $con['db_name']
                )
            );

            $resource = $GLOBALS['mysqli'][@$con['db_alias'] ? $con['db_alias'] : $con['db_name']];
        }

        else if ( is_string($resource) )
        {
            foreach ( (array) @ $GLOBALS['database'] as $con )
                $GLOBALS['mysqli'][@$con['db_alias'] ? $con['db_alias'] : $con['db_name']] = array(
                    'db_name' => $con['db_name'],
                    'db_con' => @ new \mysqli(
                        $con['db_host'],
                        $con['db_user'],
                        $con['db_pass'],
                        $con['db_name']
                    )
                );

            $resource = $GLOBALS['mysqli'][$resource];
        }

        // Link the connection resource
        $this->db_con = $resource['db_con'];
        $this->db_name = $resource['db_name'];

        // Offer a shortcut by passing a pull request in the constructor
        if(!is_null($pull))
        {
            if( @ empty($pull['limit']) )
                $pull['limit'] = '1';

            $this->pull($pull);
        }
    }

    /* Magic methods & managers
        ------------------------------------------------------------ */

    // Magic selectors for organizing data
    public function __set($name, $value)
    {
        $this->data[$name][] = array(
            'value' => $value,
            'ai' => null
        );
    }

    public function __get($name)
    {
        if(sizeof(@$this->data[$name]) < 2)
            $data = !$this->data[$name][0]['del']
                ? $this->data[$name][0]['value']
                : null;
        else
            foreach($this->data[$name] as $value)
                $data[] = !$value['del']
                    ? $value['value']
                    : null;

        return $data;
    }

    // Construct an associative array of the queried data
    public function get()
    {
        $stack = array();

        foreach(@$this->data as $colName => $rows)
            foreach(@$rows as $i => $rowData)
                @$stack[$i][$colName] = @$rowData['value'];

        $this->get = $stack;

        return $stack;
    }

    // Change an existing value
    public function update($column, $filter, $value)
    {
        // Escape apostrophes
        $value = str_replace('\'', '\'\'', $value);

        // Surround value by quotes
        if ( $value != 'NOW()' ) $value = "'{$value}'";

            // If $filter is an integer, we assume it's an index from the array matrix
        if(is_int($filter))
        {
            $this->data[$column][$filter]['value'] = $value;
            $this->data[$column][$filter]['update'] = true;
        }

        // If $filter is a string, we generate an UPDATE query for the queryCollection
        if(is_string($filter))
            foreach($this->schema as $schema)
                if($schema['COLUMN_NAME'] == $column)
                    $this->queryCollection .= "UPDATE {$schema['TABLE_NAME']} SET `{$column}` = {$value} WHERE {$filter};\n\n";
        
        $this->push();
    }

    // Delete an entire record (by auto increment)
    public function delete($index)
    {
        foreach($this->data as $column => $data)
            foreach($data as $colData => $colValue)
                if(@$colValue['ai'] == $index)
                    $this->data[$column][$colData]['del'] = true;
    }

    // Insert new data to the entity
    public function insert($insert, $auto_push = false, $ai = array())
    {
        // Iterate every insertion option
        foreach($insert as $col => $data)
        {
            $data = mysqli_real_escape_string($this->db_con, $data);

            // Split the table & column name
            $opt = explode('.', $col);

            // First create the auto increment (only once)
            if(!in_array($opt[0], $ai))
            {
                $ai[] = $opt[0];

                $this->data["{$opt[0]}_id"][] = array(
                    'value' => false,                           // Data
                    'ai' => false,                              // Auto Increment (int)
                    'update' => false,                          // Update record (bool)
                    'del' => false,                             // Delete (bool)
                    'insert' => true                            // Insert (bool)
                );
            }

            // Create all the values
            $this->data[$opt[1]][] = array(
                'value' => $data, // Data
                'ai' => false,                              // Auto Increment (int)
                'update' => false,                          // Update record (bool)
                'del' => false,                             // Delete (bool)
                'insert' => true                            // Insert (bool)
            );
        }

        // Push automatically if argument is true
        if($auto_push)
            $this->push();
    }

    // Return the entire data array
    public function dumpData()
    {
        return $this->data;
    }

    // Return the entire schema
    public function dumpSchema()
    {
        return $this->schema;
    }

    /* MySQLi functionality
        ------------------------------------------------------------ */

    // Execute a query
    public function execute($query)
    {
        return $this->query($query);
    }

    // Execute & parse a query
    protected function query($query, $map = false)
    {
        // If $map is true, empty data array
        if($map) $this->data = $this->schema = array();

        // Return false if the query failed for whatever reason
        if (!@$this->db_con->multi_query($query))
            return false;

        // Fetch the queried data & if $map variable was set to true; map into the data array
        while ($this->db_con->more_results() && $this->db_con->next_result());
        if ($result = $this->db_con->store_result())
            foreach($result->fetch_all(MYSQL_ASSOC) as $records => $columns)
                if($map)
                    foreach($columns as $column => $value)
                    {
                        $c = array_keys($columns);

                        $this->data[$column][] = @array(
                            'value' => $value,                          // Data
                            'ai' => @$columns[$c[0]],                   // Auto Increment (int)
                            'update' => false,                          // Update record (bool)
                            'del' => false,                             // Delete (bool)
                            'insert' => false                           // Insert (bool)
                        );
                    }
                else
                    $construct[] = $columns;

        if($map)
            return $this->data;
        else
            return sizeof(@$construct) > 1
                ? @$construct
                : @$construct[0];
    }

    /* Functionality for PUSHING data
        ------------------------------------------------------------ */

    // Push a mapped entity
    public function push($update = '', $insert = '', $delete = '', $alter = '')
    {
        // Every column
        foreach($this->data as $columns => $rowData)
            foreach($rowData as $columnData)
            {
                // Modify column's datatype
                if(gettype(@$columnData['value']) == 'double' && @$this->schema[$columns]['DATA_TYPE'] == 'int')
                {
                    $this->schema[$columns]['modified'] = true;
                    $this->schema[$columns]['DATA_TYPE'] = 'double';
                }

                if(gettype(@$columnData['value']) == 'string' && (@$this->schema[$columns]['DATA_TYPE'] == 'int' || @$this->schema[$columns]['DATA_TYPE'] == 'double'))
                {
                    $this->schema[$columns]['modified'] = true;
                    $this->schema[$columns]['DATA_TYPE'] = 'varchar(255)';
                }

                // Update value
                if($columnData['update'])
                    $update .= @"UPDATE `{$this->schema[$columns]['TABLE_NAME']}` SET `$columns`='{$columnData['value']}' WHERE `{$this->schema[$columns]['TABLE_NAME']}_id`='{$columnData['ai']}';\n";
            }

        // Every auto increment
        foreach($this->tables as $tables)
            foreach((array) @$this->data["{$tables}_id"] as $i => $data)
            {
                // Deletion
                if($data['del'])
                    $delete .= "DELETE FROM `{$tables}` WHERE `{$tables}_id` = '{$data['ai']}';\n";

                // Insertion
                $insertCols = $insertData = '';

                if($data['insert'])
                {
                    foreach ($this->data as $col => $colData)
                        foreach ($colData as $j => $rowData)
                            if ($i == $j && $col != @"{$tables}_id" && @$rowData['insert'] && @$this->schema[$col]['TABLE_NAME'] == $tables)
                            {
                                $insertCols .= "{$tables}.{$col}, ";
                                $insertData .= "'{$rowData['value']}', ";

                                break;
                            }

                    $insertCols = substr($insertCols, 0, -2);
                    $insertData = substr($insertData, 0, -2);

                    if (!empty($insertCols) && !empty($insertData))
                        $insert .= "INSERT INTO `{$tables}` ($insertCols) VALUES ($insertData);\n";
                }
            }

        // Every schema
        foreach($this->schema as $c => $s)
            if(@$s['modified'])
                $alter .= "ALTER TABLE `{$s['TABLE_NAME']}` MODIFY `{$c}` {$s['DATA_TYPE']};\n";

        // DEVELOPERS ONLY: Meant for debugging query
        if($this->debug)
            echo "<pre>{$alter}{$insert}{$update}{$delete}{$this->queryCollection}</pre>\n\n";

        $this->execute("{$alter}{$insert}{$update}{$delete}{$this->queryCollection}");

        // Pull again
        $this->pull($this->pullRequest);

        return true;
    }

    /* Functionality for PULLING data
        ------------------------------------------------------------ */

    // Pull & map an entity
    public function pull($options = array(), $get = false)
    {
        $this->data = $this->schema = $this->tables = array();

        $queryBuild = '';

        // Remember table & column
        if ( @!empty($options['tables']) && empty($this->store_tables) )
            $this->store_tables = $options['tables'];

        if ( @!empty($options['columns']) && empty($this->store_columns) )
            $this->store_columns = $options['columns'];

        if( @empty($options['tables']) && !empty($this->store_tables) )
            $options['tables'] = $this->store_tables;

        if( @empty($options['columns']) && !empty($this->store_columns) )
            $options['columns'] = $this->store_columns;

        // Correct as many mistakes left in the options array as possible
        $options = $this->correctPullOptions($options);

        if ( @ $GLOBALS['mycrud_schema'] !== '0' )
            $queryBuild .= $this->build_Insert(@$options['tables']);

        if ( @ $GLOBALS['mycrud_schema'] !== '0' )
            $queryBuild .= $this->build_AddCols($options['columns']);

        $queryBuild .= "SELECT {$this->build_SelectCols($options['columns'], $options['tables'])} FROM {$options['tables'][0]}\n\n";

        $queryBuild .= "{$this->build_SpecifyJoins($options['tables'])}\n\n";

        if(!@empty($options['filter'])) $queryBuild .= "WHERE {$options['filter']}\n\n";
        if(!@empty($options['order'])) $queryBuild .= "ORDER BY {$options['order']}\n\n";
        if(!@empty($options['limit'])) $queryBuild .= "LIMIT {$options['limit']}\n\n";
        if(!@empty($options['group'])) $queryBuild .= "GROUP BY {$options['group']}\n\n";

        // Echo your query build if debug has been switched true
        if($this->debug)
            echo "<pre>{$queryBuild}</pre>\n\n";

        // Execute query. Set $map to true
        $data = $this->query((string) $queryBuild, true);

        // Request the schema of every used column
        $this->buildSchema($options['columns']);

        $this->storeTables($options['tables']);
        // Let the object remember the last pull request (leave out every insert commands)
        foreach($options as $key => $value)
            if(!in_array($key, array('insert', 'insert_push')))
                $this->pullRequest[$key] = $value;

        $this->get = $this->get();

        // Check for the insert option & run an insert command
        if(!@empty($options['insert']))
            $this->insert($options['insert']);
        elseif(!@empty($options['insert_push']))
            $this->insert($options['insert_push'], true);

        // If $get is set true, return with get() method
        if($get)
            return $this->get();
        else
            return $data;


    }

    protected function correctPullOptions($options)
    {
        // Assume that a mere tables string is a one-lengthy array
        if(is_string($options['tables']))
            $options['tables'] = array($options['tables']);

        // Assume that a mere columns string is a one-lengthy array
        if(is_string($options['columns']))
            $options['columns'] = array($options['columns']);

        return $options;
    }

    // Save the array of table names
    protected function storeTables($tables)
    {
        foreach($tables as $table)
        {
            $table = explode(':', $table);

            $this->tables[] = $table[0];
        }
    }

    // Build a schema of used columns
    protected function buildSchema($columns)
    {
        foreach ($columns as $n => $c)
        {
            $column = explode('.', $c);

            $this->schema[$column[1]] =
                $this->execute("
                    SELECT * FROM INFORMATION_SCHEMA.COLUMNS
                    WHERE TABLE_SCHEMA = '{$this->db_name}' AND TABLE_NAME = '{$column[0]}' AND COLUMN_NAME = '{$column[1]}';
                ");
        }

        return $this->schema;
    }

    // Generate queries for specifying certain types of joins
    protected function build_SpecifyJoins($tables, $queryBuild = '')
    {
        foreach(array_slice($tables, 1) as $table)
        {
            $table = explode(':', $table);

            // If it's a join
            if(sizeof($table) == 1)
                $queryBuild .= "JOIN {$table[0]}\n";
            else
                $queryBuild .= "LEFT JOIN  {$table[0]} ON {$table[1]}\n";
        }

        return $queryBuild;
    }

    // Generate queries for selecting columns
    protected function build_SelectCols($columns, $tables, $queryBuild = '')
    {
        // First select the auto increments of every table
        foreach($tables as $table)
        {
            $table = explode(':', $table);

            $queryBuild .= "{$table[0]}.{$table[0]}_id, ";
        }

        // Select given columns
        foreach($columns as $col)
            $queryBuild .=
                join('.', array_slice(explode('.', $col), 0, 2))
                . (end($columns) != $col ? ', ' : null);            // Separator

        return $queryBuild;
    }

    // Generate queries for inserting new tables
    protected function build_Insert($tables, $queryBuild = '')
    {
        // Create tables if they don't exist
        foreach($tables as $tableName)
        {
            $tableName = explode(':', $tableName);

            if ( $GLOBALS['mycrud_schema'] === 1 )
                $queryBuild .= "CREATE TABLE IF NOT EXISTS `{$tableName[0]}` (
                               `{$tableName[0]}_id` INT NOT NULL AUTO_INCREMENT,
                               PRIMARY KEY (`{$tableName[0]}_id`));\n\n";
        }

        return $queryBuild;
    }

    // Generate queries for inserting columns
    protected function build_AddCols($columns, $queryBuild = '')
    {
        // Create table columns if they don't exist
        foreach($columns as $column)
        {
            $col = explode('.', $column);

            $col[2] = !@empty($col[2]) ? $col[2] : 'INT(255) NULL';

            if($col[1] != '*' && $GLOBALS['mycrud_schema'] === 1 )
                $queryBuild .= "DROP PROCEDURE IF EXISTS add_column;
                                CREATE PROCEDURE add_column()

                                BEGIN
                                  IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS
                                      WHERE TABLE_SCHEMA = '{$this->db_name}' AND TABLE_NAME = '{$col[0]}' AND COLUMN_NAME = '{$col[1]}') THEN
                                    ALTER TABLE `{$col[0]}` ADD COLUMN `{$col[1]}` {$col[2]};
                                  END IF;
                                END;

                                CALL add_column;

                                DROP PROCEDURE IF EXISTS add_column;\n\n";
        }

        return $queryBuild;
    }

    /* Miscellaneous
    ------------------------------------------------------------ */

    // Switch debug mode. Mainly used for echo 'ing query constructions
    public function debug($bool)
    {
        $this->debug = $bool;
    }
}
