<?php

require_once 'PHPCycle_Assets/Assets.php';

/**
 * Route extension
 *
 * @param $para
 * @param $attr
 * @return bool
 */
$GLOBALS['route_extensions']['asset'] = function($para, $attr)
{
    $bool_minified = ! (@$attr['minified'] == 'false');

    /**
     * Recursively create asset folders
     */
    foreach ( array_merge([ '_less', 'css', 'font', 'img', 'js', 'txt' ], @ (array) $GLOBALS['asset_folders']) as $i )
        if ( ! is_dir("../src/{$GLOBALS['phpcycle']['project']}/view/assets/{$i}")  )
            mkdir("../src/{$GLOBALS['phpcycle']['project']}/view/assets/{$i}", 0777, true);

    /**
     * Filter out illegal characters from the parameters
     */
    $illegal_pattern = "(\.\./)";

    foreach ( $para as $i => $str )
        while ( preg_match("|{$illegal_pattern}|", $para[$i]) )
            $para[$i] = preg_replace("|{$illegal_pattern}|", '', $para[$i]);

    /**
     * Rendering asset files
     */
    $str_content = '';

    switch ( $attr['asset'] )
    {
        case 'svg' :
            $str_content .= "<?xml version=\"1.0\" standalone=\"no\"?>
                             <!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\"
                             \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">";

            $str_content .= PHPCycle_Assets::svg(urldecode($para[0]));

            if ( $str_content )
                $attr['content-type'] = 'image/svg+xml';
        break;

        case 'pdf' :
            $str_content .= PHPCycle_Assets::pdf(urldecode($para[0]));

            $attr['content-type'] = 'Content-type:application/pdf';
        break;

        case 'txt' :
            $str_content .= PHPCycle_Assets::txt(urldecode($para[0]));

            foreach ( (array) @ explode('&', $para[1]) as $file )
                $str_content .= PHPCycle_Assets::txt(urldecode($file), ' ');
        break;

        case 'font' :
            $str_content .= PHPCycle_Assets::font(urldecode($para[0]));
        break;

        case 'img' :
            $str_content .= PHPCycle_Assets::img(urldecode($para[0]));

            if ( $str_content )
                $attr['content-type'] = 'image/' . pathinfo(urldecode($para[0]), PATHINFO_EXTENSION);
        break;

        case 'js' :
            $str_content .= PHPCycle_Assets::js(urldecode($para[0]), $bool_minified);

            foreach ( (array) @ explode('&', $para[1]) as $file )
                $str_content .= PHPCycle_Assets::js(urldecode($file), $bool_minified, ' ');
        break;

        case 'css' :
            $str_content .= PHPCycle_Assets::css(urldecode($para[0]), $bool_minified);

            foreach ( (array) @ explode('&', $para[1]) as $file )
                $str_content .= PHPCycle_Assets::css(urldecode($file), $bool_minified, ' ');
        break;
    }

    /**
     * Set browser cache, header & exit with content if files were found,
     * otherwise return this function to allow alternative functions
     */
    if ( strlen($str_content) < 1 )
        return false;

    else
    {
        /**
         * Set browser cache
         */
        if ( isset($attr['browser_cache']) )
        {
            @ header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $attr['browser_cache']) . ' GMT');
            @ header('Pragma: cache');
            @ header('Cache-Control: max-age=' . $attr['browser_cache']);
            @ header('Last-Modified: '.gmdate('D, d M Y H:i:s', time() - (60*60*24)).' GMT');
        }

        /**
         * Set content-type
         */
        if ( @ $attr['content-type'] )
            header("Content-Type: {$attr['content-type']}");

        exit($str_content);
    }
};

/**
 * (c) interpersona.nl
 * Retrieves 3 latest uploaded PDF files from main_site/view/assets/pdf/[A/B/C] directory
 * -sorts by name
 *
 * @param $para
 * @param $attr
 */
$GLOBALS['route_extensions']['pdf_download'] = function($para, $attr)
{
    $list = [
        'A' => "../src/{$GLOBALS['phpcycle']['project']}/view/assets/pdf/A/*",
        'B' => "../src/{$GLOBALS['phpcycle']['project']}/view/assets/pdf/B/*",
        'C' => "../src/{$GLOBALS['phpcycle']['project']}/view/assets/pdf/C/*",
    ];

    foreach ( $list as $key => $i )
    {
        $files = glob($i);
        $files = array_combine($files, array_map("filemtime", $files));
        arsort($files);

        @ $list[$key] = "{$key}/" . basename(key($files));
    }

    Memc::set([ ['pdf_download', $list] ]);

    return $list;
};