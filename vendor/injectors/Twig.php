<?php

require_once 'Twig/Autoloader.php';


class Twig
{
    public static function render($file, $para = array(), $Memcache = null)
    {
        /**
         * Default properties in $para
         */
        $para['session'] = @ $_SESSION;

        /**
         * Use Memcache in case $Memcache is set
         */
        if ( MEMCACHE && ! is_null($Memcache) )
        {
            /**
             * Swap between three scenarios:
             * 1. @string $Memcache: Sets Memcache with specified key for unlimited time
             * 2. @int $Memcache: Sets Memcache with key PROJECT_FILENAME with specified time
             * 3. @array: [ KEY, EXPIRE_TIME ]
             */
            $mem_title = is_array($Memcache) ? $Memcache[0] :
                (is_string($Memcache) ? $Memcache : "{$GLOBALS['phpcycle']['project']}_{$file}");

            $mem_expire = is_array($Memcache) ? $Memcache[1] : (is_int($Memcache) ? $Memcache : 0);

            $content = Memc::get($mem_title);

            if ( $content !== false )
                return $content;

            else
            {
                $content = self::render( $file, $para );

                Memc::set([
                    [ $mem_title, $content, $mem_expire, MEMCACHE_COMPRESSED ]
                ]);

                return $content;
            }
        }

        /**
         * Register Twig and set up file system & environment
         */
        \Twig_Autoloader::register();

        $Twig_loader = new Twig_Loader_Filesystem("../src/{$GLOBALS['phpcycle']['project']}/view/templates/");
        $Twig = new \Twig_Environment( $Twig_loader, [ 'debug' => true ] );

        /**
         * Add Twig extensions
         */
        $Twig->addExtension(new Twig_Extension_Debug());

        /**
         * Load Twig:
         *  Functions: GLOBALS['twig_functions']
         *  Filters: GLOBALS['twig_filters']
         */
        foreach ( (array) @ $GLOBALS['twig_functions'] as $name => $function )
            $Twig->addFunction(new Twig_SimpleFunction($name, $function));

        foreach ( (array) @ $GLOBALS['twig_filters'] as $name => $function )
            $Twig->addFilter(new Twig_SimpleFilter($name, $function));

        /**
         * Finally load & render template
         */
        $Twig->loadTemplate($file);
        return $Twig->render($file, $para);
    }
}