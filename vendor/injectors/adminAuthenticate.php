<?php

$GLOBALS['route_extensions']['session_start'] = function()
{
    session_start();
};

$GLOBALS['route_extensions']['adminAuthenticate'] = function()
{
    PHPCycle::load_project('admin');

    /**
     * Logon session
     * Mandatory elements:
     * [
     *      id : int,
     *      email : string,
     *      password : string
     * ]
     */
    if ( @ is_array($_SESSION['logon']) )
    {
        $adminModel = new adminModel(false);
        $adminLogin = $adminModel->login( $_SESSION['logon']['email'], $_SESSION['logon']['password'] );

        if ( $adminLogin === false )
            unset($_SESSION['logon']);
        else
            $_SESSION['logon'] = $adminLogin;
    }

    else
    {
        $_SESSION['logon'] = false;

        PHPCycle::load_project('admin', 'controller');

        exit ( (new dashboardController())->index([]) );
    }
};